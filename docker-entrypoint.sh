#!/bin/sh
set -e

echo "Starting borg backup container ..."

if [ ! -f "$BORG_REPO/config" ]; then
    echo "borg repository '${BORG_REPO}' does not exists. Running borg init."
    borg init -e repokey | true
fi

echo "Setup backup cron job with cron expression BACKUP_CRON: ${BACKUP_CRON}"
echo "${BACKUP_CRON} /usr/sbin/backup >> /var/log/cron.log 2>&1" > /var/spool/cron/crontabs/root

# Make sure the file exists before we start tail
touch /var/log/cron.log

# start the cron deamon
crond

echo "Container started."

tail -fn0 /var/log/cron.log