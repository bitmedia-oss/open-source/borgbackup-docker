#!/bin/sh

echo "Removing old container names 'backup-test' if exists"
docker rm -f -v backup-test || true

echo "Start backup-test container. Backup of testbackupdata to repository backuprepo every minute"
docker run --privileged --name backup-test \
-e BORG_REPO="backup" \
-e BORG_PASSPHRASE="MyverySuperlongandSuperStrongPassphrase" \
-e BACKUP_CRON="*/1 * * * *" \
-e BACKUP_NAME="Test" \
-e PRUNE_DAILY="7" \
-e PRUNE_WEEKLY="4" \
-e PRUNE_MONTLY="6" \
-v testbackupdata:/data \
-v backuprepo:/backup \
-t borgbackup
