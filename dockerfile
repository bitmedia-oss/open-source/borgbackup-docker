FROM alpine:edge
MAINTAINER Michael Holasek <michael.holasek@bitmedia.at>

ENV TZ=Europe/Vienna

RUN apk add --no-cache borgbackup openssh sshfs fuse \ 
  && apk add --no-cache --update tzdata \
  && rm -rf /var/cache/apk/*

ENV BORG_REPO="/backup"
ENV BORG_PASSPHRASE="MyverySuperlongandSuperStrongPassphrase"
ENV BACKUP_CRON="0 0 * * *"
ENV BACKUP_NAME="Test"
#ENV BORG_RSH='ssh -i /path/to/private/key'
ENV PRUNE_DAILY=7
ENV PRUNE_WEEKLY=4
ENV PRUNE_MONTHLY=6

#mount the volume you want to backup to data
VOLUME /data

COPY backup.sh /usr/sbin/backup
RUN chmod +x /usr/sbin/backup

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat

RUN touch /var/log/cron.log

WORKDIR "/"

ENTRYPOINT ["docker-entrypoint.sh"]